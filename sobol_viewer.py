# Copyright (c) 2023, Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement (INRAE)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
import os
import matplotlib
matplotlib.use('Qt5Agg')

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pylab as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import numpy as np


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=500, height=400, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        spec = matplotlib.gridspec.GridSpec(ncols=1, nrows=4,
                         width_ratios=[1], wspace=0.5,
                         hspace=0.5, height_ratios=[1,1,1,1])
        self.axes1 = self.fig.add_subplot(spec[0])
        self.axes2 = self.fig.add_subplot(spec[1])
        self.axes3 = self.fig.add_subplot(spec[2])
        self.axes4 = self.fig.add_subplot(spec[3])
        super(MplCanvas, self).__init__(self.fig)

    def set_colorbar(self, colormap):
        self.normalize = mcolors.Normalize(vmin=0, vmax=1)
        scalarmappable1 = cm.ScalarMappable(norm=mcolors.NoNorm(), cmap=colormap)
        self.cbar1 = self.fig.colorbar(scalarmappable1, ax = self.axes1)
        scalarmappable2 = cm.ScalarMappable(norm=mcolors.NoNorm(), cmap=colormap)
        self.cbar2 = self.fig.colorbar(scalarmappable2, ax = self.axes2)
        scalarmappable3 = cm.ScalarMappable(norm=mcolors.NoNorm(), cmap=colormap)
        self.cbar3 = self.fig.colorbar(scalarmappable3, ax = self.axes3)
        scalarmappable4 = cm.ScalarMappable(norm=mcolors.NoNorm(), cmap=colormap)
        self.cbar4 = self.fig.colorbar(scalarmappable4, ax = self.axes4)

class MplCanvas2(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=500, height=400, dpi=100):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        spec = matplotlib.gridspec.GridSpec(ncols=1, nrows=1,
                         width_ratios=[1], wspace=0.5,
                         hspace=0.5, height_ratios=[1])
        self.axes1 = self.fig.add_subplot(spec[0])
        super(MplCanvas2, self).__init__(self.fig)

    def set_colorbar(self, colormap):
        self.normalize = mcolors.Normalize(vmin=0, vmax=1)
        scalarmappable1 = cm.ScalarMappable(norm=mcolors.NoNorm(), cmap=colormap)
        self.cbar1 = self.fig.colorbar(scalarmappable1, ax = self.axes1)

class Sobol():
    def __init__(self, study_path, sample_size):
        self.values = {}
        self.values_tot = {}
        self.variances = {}
        self.means = {}
        self.ci = {}
        self.ci_tot = {}
        self.sum1 = {}
        self.sum_tot = {}
        self.outputs = []
        self.nb_timesteps = 0
        self.vect_size = 0
        for root, dirs, files in os.walk(study_path+'/results'):
            for file in files:
                if ("results." in file and "_sobol0" in file):
                    out = file.split("_sobol0")[0].split("results.")[1]
                    if out not in self.outputs:
                        self.outputs.append(out)
        if len(self.outputs) == 0:
            print ("ERROR: no results found for sobol indices")
            return
        for root, dirs, files in os.walk(study_path+'/results'):
            for file in files:
                if file.startswith('results.'+self.outputs[0]+'_sobol0'):
                    self.nb_timesteps += 1
        form = '0'+str(int(np.ceil(np.log10(self.nb_timesteps+1))))+'d'
        for out in self.outputs:
            self.values[out] = []
            self.values_tot[out] = []
            for t in range(self.nb_timesteps):
                values = []
                p=0
                p_found = True
                t_f = format(t+1, form)
                while p_found :
                    name = study_path+'/results/results.'+out+'_sobol_tot'+str(p)+'.'+t_f
                    if not os.path.isfile(name):
                        p_found = False
                    else:
                        print ("read "+name)
                        values.append([])
                        with open(name) as f:
                            lines = f.read().splitlines()
                            self.vect_size = len(lines)
                            for line in lines:
                                values[p].append(float(line))
                        p += 1
                self.nb_param = p
                self.values[out].append(np.array(values))
            self.values_tot[out] = []
            for t in range(self.nb_timesteps):
                values_tot = []
                t_f = format(t+1, form)
                for p in range(self.nb_param):
                    name = study_path+'/results/results.'+out+'_sobol'+str(p)+'.'+t_f
                    print ("read "+name)
                    values_tot.append([])
                    with open(name) as f:
                        for v in range(self.vect_size):
                            values_tot[p].append(float(f.readline()))
                self.values_tot[out].append(np.array(values_tot))

        self.y, self.x = np.mgrid[slice(0.5, self.nb_param+0.5 + 1, 1),
                                  slice(0.5, self.vect_size+0.5 + 1, 1)]

        # sums
        for out in self.outputs:
            self.sum1[out] = []
            self.sum_tot[out] = []
            for t in range(self.nb_timesteps):
                sum1 = []
                sum_tot = []
                for v in range(self.vect_size):
                    sum1.append(0.0)
                    sum_tot.append(0.0)
                    for p in range(self.nb_param):
                        sum1[v] += self.values[out][t][p][v]
                        sum_tot[v] += self.values_tot[out][t][p][v]
                self.sum1[out].append(np.array(sum1))
                self.sum_tot[out].append(np.array(sum_tot))

        for out in self.outputs:
            self.means[out] = []
            for t in range(self.nb_timesteps):
                means = []
                t_f = format(t+1, form)
                name = study_path+'/results/results.'+out+'_mean.'+t_f
                print ("read "+name)
                with open(name) as f:
                    for v in range(self.vect_size):
                        means.append(float(f.readline()))
                self.means[out].append(np.array(means))

        self.normalize2 = {}
        for out in self.outputs:
            self.normalize2[out] = mcolors.Normalize(vmin=min(np.min(self.means[out][:][:]),0.0), vmax=np.max(self.means[out][:][:]))

        for out in self.outputs:
            self.variances[out] = []
            for t in range(self.nb_timesteps):
                variances = []
                t_f = format(t+1, form)
                name = study_path+'/results/results.'+out+'_variance.'+t_f
                print ("read "+name)
                with open(name) as f:
                    for v in range(self.vect_size):
                        variances.append(float(f.readline()))
                self.variances[out].append(np.array(variances))

        self.y2, self.x2 = np.mgrid[slice(0, 2, 1),
                                    slice(0.5, self.vect_size+0.5 + 1, 1)]

        for out in self.outputs:
            self.ci[out] = []
            self.ci_tot[out] = []
            for t in range(self.nb_timesteps):
                ci = []
                ci_tot = []
                for p in range(self.nb_param):
                    ci.append([])
                    ci_tot.append([])
                    for v in range(self.vect_size):
                        i = self.values[out][t][p][v]
                        j = self.values_tot[out][t][p][v]
                        if i < 1:
                            ci[p].append(np.tanh(0.5*np.log10((1+i)/(1-i))+1.96/np.sqrt(sample_size-3)) - np.tanh(0.5*np.log10((1+i)/(1-i))-1.96/np.sqrt(sample_size-3)))
                        else:
                            ci[p].append(0.0)
                        if j > 0:
                            ci_tot[p].append(1-np.tanh(0.5*np.log10((2-j)/j)-1.96/np.sqrt(sample_size-3)) - (1-np.tanh(0.5*np.log10((2-j)/j)+1.96/np.sqrt(sample_size-3))))
                        else:
                            ci_tot[p].append(0.0)
                self.ci[out].append(np.array(ci))
                self.ci_tot[out].append(np.array(ci_tot))

        print("nb_param      : ",self.nb_param)
        print("vect_size     : ",self.vect_size)
        print("outputs       : ",self.outputs)
        print("nb timesteps  : ",self.nb_timesteps)
        print("nb iterations : ",sample_size)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        print( 'Number of arguments:', len(sys.argv), 'arguments.')
        print( 'Argument List:', str(sys.argv))
        #self.nb_param = 12
        #self.nb_timesteps = 15
        #self.nb_sections = 602
        #self.out = ["Q","Z"]
        self.timestep = 1.0
        self.path = "."
        self.sample_size = 3
        if len(sys.argv) > 1:
            self.path = str(sys.argv[1])
        if len(sys.argv) > 2:
            self.timestep = float(sys.argv[2])
        if len(sys.argv) > 3:
            self.sample_size = int(sys.argv[3])

        self.sobol = Sobol(self.path, self.sample_size)

        self.colormap = cm.seismic

        layout = MyTableWidget(self)
        self.setCentralWidget(layout)

        self.show()

class MyTableWidget(QtWidgets.QWidget):

    def __init__(self, parent):
        super(QtWidgets.QWidget, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.sobol = parent.sobol
        self.timestep = parent.timestep
        self.colormap = parent.colormap

        self.slider2Value = 1
        self.paramValue = 0
        self.orderValue = 0
        # Initialize tab screen
        self.tabs = QtWidgets.QTabWidget()
        self.tab1 = Tab1(self)
        self.tab2 = Tab2(self)
        #self.tabs.resize(300,200)

        # Add tabs
        self.tabs.addTab(self.tab1,"Parameter")
        self.tabs.addTab(self.tab2,"Global")

        # Create tabs
        self.tab1.setLayout(self.tab1.layout)
        self.tab2.setLayout(self.tab2.layout)
        self.tabs.currentChanged.connect(self.update_plots)

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def update_plots(self, index):
        self.tabs.widget(index).cb2.setCurrentIndex(self.paramValue)
        self.tabs.widget(index).slider2.setValue(self.slider2Value)
        self.tabs.widget(index).cb.setCurrentIndex(self.orderValue)
        self.tabs.widget(index).update_plot()

    def update_t(self, value):
        self.slider2Value = value

    def update_param(self, value):
        self.paramValue = value

    def update_order(self, value):
        if value == 0 or value == 2:
            self.orderValue = 0
        else:
            self.orderValue = 1

class Tab1(QtWidgets.QWidget):

    def __init__(self, parent):
        super(QtWidgets.QWidget, self).__init__(parent)
        self.parent = parent
        self.layout = QtWidgets.QVBoxLayout(self)
        self.sobol = parent.sobol
        self.timestep = parent.timestep
        self.colormap = parent.colormap

        self.sc = MplCanvas(self, width=500, height=500, dpi=100)
        self.sc.axes4.set_xlabel("Section")
        self.sc.axes1.set_ylabel("Sobol Indices")
        self.sc.axes2.set_ylabel("Mean")
        self.sc.axes3.set_ylabel("Variance")
        self.sc.axes4.set_ylabel("Confidence Interval")
        has_legend = False

        # Create toolbar
        toolbar = NavigationToolbar(self.sc, self)
        self.layout.addWidget(toolbar)

        self.sc.set_colorbar(self.colormap)
        self.sc.axes1.set_xlim(0.5,self.sobol.vect_size+0.5)
        #self.sc.axes1.set_ylim(0.5,self.sobol.nb_param+0.5)
        self.sc.axes1.set_ylim(0.0,1.0)
        #self.sc.axes1.set_yticks(range(1,self.sobol.nb_param+1))
        self.sc.axes1.set_yticks([])
        self.sc.axes1.set_title("Random", fontsize = 20)
        self.sc.axes2.set_xlim(0.5,self.sobol.vect_size+0.5)
        self.sc.axes2.set_ylim(0.0,1.0)
        self.sc.axes2.set_yticks([])
        self.sc.axes3.set_xlim(0.5,self.sobol.vect_size+0.5)
        self.sc.axes3.set_ylim(0.0,1.0)
        self.sc.axes3.set_yticks([])
        self.sc.axes4.set_xlim(0.5,self.sobol.vect_size+0.5)
        self.sc.axes4.set_ylim(0.0,1.0)
        self.sc.axes4.set_yticks([])

        self.hbox0 = QtWidgets.QHBoxLayout()
        self.vbox0 = QtWidgets.QVBoxLayout()

        # Create combo box order
        self.hbox = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel("Order")
        self.hbox.addWidget(self.label)
        self.cb = QtWidgets.QComboBox()
        self.cb.addItems(["Sobol 1", "Sobol total"])
        self.cb.currentIndexChanged.connect(self.update_plot)
        self.cb.currentIndexChanged.connect(parent.update_order)
        self.hbox.addWidget(self.cb)

        self.vbox0.addLayout(self.hbox)

        # Create combo box parameter
        self.hbox2 = QtWidgets.QHBoxLayout()
        self.label2 = QtWidgets.QLabel("Parameter")
        self.hbox2.addWidget(self.label2)
        self.cb2 = QtWidgets.QComboBox()
        self.cb2.addItems(self.sobol.outputs)
        self.cb2.currentIndexChanged.connect(self.update_plot)
        self.cb2.currentIndexChanged.connect(parent.update_param)
        self.hbox2.addWidget(self.cb2)
        self.slider = QtWidgets.QSlider(Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(self.sobol.nb_param)
        self.slider.setValue(1)
        self.slider.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.slider.setTickInterval(1)
        self.hbox2.addWidget(self.slider)

        self.vbox0.addLayout(self.hbox2)

        # Create slider
        self.hbox3 = QtWidgets.QHBoxLayout()
        self.label3 = QtWidgets.QLabel("Timestep")
        self.hbox3.addWidget(self.label3)
        self.slider2 = QtWidgets.QSlider(Qt.Horizontal)
        self.slider2.setMinimum(0)
        self.slider2.setMaximum(self.sobol.nb_timesteps-1)
        self.slider2.setValue(1)
        self.slider2.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.slider2.setTickInterval(1)
        self.hbox3.addWidget(self.slider2)

        #layout.addWidget(self.slider)
        self.vbox0.addLayout(self.hbox3)
        self.hbox0.addLayout(self.vbox0)

        #layout.addLayout(self.hbox0)
        self.layout.addWidget(self.sc)
        self.layout.addLayout(self.hbox0)

        self.update_plot()

        # Setup a trigger the redraw by calling update_plot.
        self.slider2.valueChanged.connect(self.update_plot)
        self.slider2.valueChanged.connect(parent.update_t)
        self.slider.valueChanged.connect(self.update_plot)

    def update_plot(self):

        t = self.slider2.value()
        o = self.cb2.currentText()
        param = self.slider.value()-1

        if self.cb.currentText() == "Sobol 1":
            out_values = self.sobol.values[o][t][param]
            ci = self.sobol.ci[o][t][param]
            self.sc.axes1.set_title("First order Sobol Indices of parameter "+str(self.slider.value())+" on output "+o+" at t="+str(t*self.timestep)+"\n", fontsize = 20)
        elif self.cb.currentText() == "Sobol total":
            out_values = self.sobol.values_tot[o][t][param]
            ci = self.sobol.ci_tot[o][t][param]
            self.sc.axes1.set_title("Total order Sobol Indices of parameter "+str(self.slider.value())+" on output "+o+" at t="+str(t*self.timestep)+"\n", fontsize = 20)
        self.sc.cbar1.update_normal(self.sc.axes1.pcolor(self.sobol.x2, self.sobol.y2, [out_values], norm=self.sc.normalize, cmap = self.colormap))
        self.sc.axes2.set_ylabel("Mean "+o)
        self.sc.axes3.set_ylabel("Variance "+o)
        self.sc.cbar2.update_normal(cm.ScalarMappable(norm=self.sobol.normalize2[o], cmap=self.colormap))
        self.sc.axes2.pcolor(self.sobol.x2, self.sobol.y2, [self.sobol.means[o][t]], norm=self.sobol.normalize2[o], cmap = self.colormap)
        self.sc.cbar3.update_normal(self.sc.axes3.pcolor(self.sobol.x2, self.sobol.y2, [self.sobol.variances[o][t]], cmap = self.colormap))
        self.sc.cbar4.update_normal(self.sc.axes4.pcolor(self.sobol.x2, self.sobol.y2, [ci], cmap = self.colormap))

        #self.sc.cbar2.update_normal(p2)
        self.sc.draw()

class Tab2(QtWidgets.QWidget):

    def __init__(self, parent):
        super(QtWidgets.QWidget, self).__init__(parent)
        self.parent = parent
        self.layout = QtWidgets.QVBoxLayout(self)
        self.sobol = parent.sobol
        self.timestep = parent.timestep
        self.colormap = parent.colormap

        self.sc = MplCanvas2(self, width=500, height=500, dpi=100)
        self.sc.axes1.set_xlabel("Section")
        self.sc.axes1.set_ylabel("Parameter")
        h = 0.5
        has_legend = False
        for p in range(self.sobol.nb_param):
          h += 1
          self.sc.axes1.hlines(h, 0.5, self.sobol.vect_size+0.5,linestyles="solid",colors="black")

        # Create toolbar
        toolbar = NavigationToolbar(self.sc, self)
        self.layout.addWidget(toolbar)

        #minvar = min(min(self.sobol.variances[o][t]) for o in self.sobol.outputs for t in range(self.sobol.nb_timesteps))
        #maxvar = max(max(self.sobol.variances[o][t]) for o in self.sobol.outputs for t in range(self.sobol.nb_timesteps))
        self.sc.set_colorbar(self.colormap)
        self.sc.axes1.set_xlim(0.5,self.sobol.vect_size+0.5)
        self.sc.axes1.set_ylim(0.5,self.sobol.nb_param+0.5)
        #self.sc.axes1.set_ylim(0.0,1.0)
        self.sc.axes1.set_yticks(range(1,self.sobol.nb_param+1))
        #self.sc.axes1.set_yticks([])
        self.sc.axes1.set_title("Random", fontsize = 20)

        self.hbox0 = QtWidgets.QHBoxLayout()
        self.vbox0 = QtWidgets.QVBoxLayout()

        # Create combo box order
        self.hbox = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel("Order")
        self.hbox.addWidget(self.label)
        self.cb = QtWidgets.QComboBox()
        self.cb.addItems(["Sobol 1", "Sobol total", "confidence interval 1", "confidence interval total"])
        self.cb.currentIndexChanged.connect(self.update_plot)
        self.cb.currentIndexChanged.connect(parent.update_order)
        self.hbox.addWidget(self.cb)

        self.vbox0.addLayout(self.hbox)

        # Create combo box parameter
        self.hbox2 = QtWidgets.QHBoxLayout()
        self.label2 = QtWidgets.QLabel("Parameter")
        self.hbox2.addWidget(self.label2)
        self.cb2 = QtWidgets.QComboBox()
        self.cb2.addItems(self.sobol.outputs)
        self.cb2.currentIndexChanged.connect(self.update_plot)
        self.cb2.currentIndexChanged.connect(parent.update_param)
        self.hbox2.addWidget(self.cb2)

        self.vbox0.addLayout(self.hbox2)

        # Create slider
        self.hbox3 = QtWidgets.QHBoxLayout()
        self.label3 = QtWidgets.QLabel("Timestep")
        self.hbox3.addWidget(self.label3)
        self.slider2 = QtWidgets.QSlider(Qt.Horizontal)
        self.slider2.setMinimum(0)
        self.slider2.setMaximum(self.sobol.nb_timesteps-1)
        self.slider2.setValue(1)
        self.slider2.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.slider2.setTickInterval(1)
        self.hbox3.addWidget(self.slider2)

        self.vbox0.addLayout(self.hbox3)
        self.hbox0.addLayout(self.vbox0)

        self.layout.addWidget(self.sc)
        self.layout.addLayout(self.hbox0)

        self.update_plot()

        # Setup a trigger the redraw by calling update_plot.
        self.slider2.valueChanged.connect(self.update_plot)
        self.slider2.valueChanged.connect(parent.update_t)

    def update_plot(self):
        t = self.slider2.value()
        o = self.cb2.currentText()
        if self.cb.currentText() == "Sobol 1":
            self.sc.axes1.set_title("First order Sobol indices at t="+str(t*self.timestep)+" on output "+o+"\n", fontsize = 20)
            out_values = self.sobol.values[o]
            self.sc.cbar1.update_normal(cm.ScalarMappable(norm=self.sc.normalize, cmap=self.colormap))
            self.sc.axes1.pcolor(self.sobol.x, self.sobol.y, out_values[t], norm=self.sc.normalize, cmap = self.colormap)
        elif self.cb.currentText() == "Sobol total":
            self.sc.axes1.set_title("Total order Sobol indices at t="+str(t*self.timestep)+" on output "+o+"\n", fontsize = 20)
            out_values = self.sobol.values_tot[o]
            self.sc.cbar1.update_normal(cm.ScalarMappable(norm=self.sc.normalize, cmap=self.colormap))
            self.sc.axes1.pcolor(self.sobol.x, self.sobol.y, out_values[t], norm=self.sc.normalize, cmap = self.colormap)
        elif self.cb.currentText() == "confidence interval 1":
            self.sc.axes1.set_title("First order confidence intervals at t="+str(t*self.timestep)+" on output "+o+"\n", fontsize = 20)
            out_values = self.sobol.ci[o]
            self.sc.cbar1.update_normal(self.sc.axes1.pcolor(self.sobol.x, self.sobol.y, out_values[t], cmap = self.colormap))
            self.sc.axes1.pcolor(self.sobol.x, self.sobol.y, out_values[t], cmap = self.colormap)
        elif self.cb.currentText() == "confidence interval total":
            self.sc.axes1.set_title("Total order confidence intervals at t="+str(t*self.timestep)+" on output "+o+"\n", fontsize = 20)
            out_values = self.sobol.ci_tot[o]
            self.sc.cbar1.update_normal(self.sc.axes1.pcolor(self.sobol.x, self.sobol.y, out_values[t], cmap = self.colormap))
            self.sc.axes1.pcolor(self.sobol.x, self.sobol.y, out_values[t], cmap = self.colormap)


        #self.sc.cbar2.update_normal(p2)
        self.sc.draw()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    app.exec_()
