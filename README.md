Usage:
python3 sobol_viewer.py "path/to/melissa/study" timestep nb_iterations

"path/to/melissa/study" is the path specified in "output_dir" of Melissa config file. Default : "."
timestep is the legth of a timestep. Default: 1.0
nb_iterations is the number of groups of simulations. Default: 3
